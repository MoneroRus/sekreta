All logos and images in this directory, and all subdirectories, are used under their respective licenses:

- [Monero](https://www.getmonero.org/press-kit/)
- [Zcash](https://z.cash/press/)
- [Bitcoin](https://en.bitcoin.it/wiki/Promotional_graphics)
- [Sekreta](../LICENSE.md)
